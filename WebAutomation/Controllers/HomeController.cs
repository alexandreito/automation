﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebAutomation.Models;

namespace WebAutomation.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View(new LoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel vm)
        {

            if (ModelState.IsValid)
            {
                if (vm.Login == "admin" && vm.Senha == "123456")
                {
                    var r = FormsAuthentication.GetAuthCookie(vm.Login, true);
                    Response.Cookies.Add(r);

                    return RedirectToAction("IndexLogado");
                }
                else
                {
                    ModelState.AddModelError("", "Login ou senha inválidos");
                }

            }

            return View(vm);
        }

        [Authorize]
        public ActionResult IndexLogado()
        {
            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }
    }
}