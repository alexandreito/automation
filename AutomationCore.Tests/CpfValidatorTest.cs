﻿using NUnit.Framework;

namespace AutomationCore.Tests
{
    [TestFixture]
    public class CpfValidatorTest
    {
        [Test]
        public void Is_nullCpf_Valid()
        {
            var validator = new CpfValidator(null);
            var r = validator.IsValid();

            Assert.AreEqual(r, false);
        }

        [Test]
        public void Is_EmptyCpf_Valid()
        {
            var validator = new CpfValidator("");
            var r = validator.IsValid();

            Assert.AreEqual(r, false);
        }

        [Test]
        public void Is_ValidCpf_Valid()
        {
            var validator = new CpfValidator("408.945.062-42");
            var r = validator.IsValid();

            Assert.AreEqual(r, true);
        }
    }
}
