﻿namespace AutomationCore
{
    public interface IValidator
    {
        bool IsValid();
    }
}
